import { RecetteDetails } from "./components/RecetteCard/RecetteDetails.js";
import { RecetteDescription } from "./components/RecetteCard/RecetteDescription.js";


const apiKey = "1";



const urlTest = "https://www.themealdb.com/api/json/v1/1/random.php"

// const paramsUrl = "&units=metric&lang=fr"


const envoyer = document.querySelector(".top-banner form")
envoyer.addEventListener("submit", rechercherRecette)
const input = document.querySelector(".top-banner input")

const rechercheAleatoire = document.querySelector(".top-banner .buttonAleatoire")
rechercheAleatoire.addEventListener("click", clickButtonAleatoire)

const urlSearchByName = "https://www.themealdb.com/api/json/v1/1/search.php?s="


function rechercherRecette(event){
    event.preventDefault()

    // recuperer la valeur du input
    let inputValue = input.value
    // requête AJAX
    const url2 = `${urlSearchByName}${inputValue}`
    console.log(url2)

    fetch(url2)
    // Le paramètre response prend la valeur de l’objet renvoyé de fetch(url). Utilisez la méthode json() pour convertir response en données JSON.
    .then((response) => response.json())
    .then(
        function(data) {
        console.log(data);
        console.log(data.meals[0].strMeal,data.meals[0].strCategory)
        //    Cette ligne de code utilise la syntaxe de décomposition ("destructuring" en anglais) en JavaScript pour extraire des valeurs d'un objet et les affecter à des variables distinctes. Plus précisément, cette ligne extrait les propriétés main, name, sys et weather de l'objet data, et les affecte respectivement aux variables du même nom.
        const {meals, strMeal, strCategory, strArea, strMealThumb} = data
        console.log(meals, meals[0].strMeal, meals[0].strCategory, meals[0].strArea, meals[0].strMealThumb, meals[0].idMeal)
        const container = document.querySelector('.cities');
        const firstCard = container.firstChild; // récupère le premier élément de la liste

        // on utilise forEach pour afficher les recettes qui ont le même nom par exemple si l'utilisateur écrit "curry" alors toutes les recettes avec le mot curry sortiront
        if (!inputValue){
            alert("Veuillez renseigner une recette");
        }else{
            meals.forEach((meal) => {
                // Créer une instance de RecetteDetails pour chaque recette
                const recetteDetails = new RecetteDetails([meal]);

                // Récupérer le contenu de la carte pour chaque recette
                const cardContent = recetteDetails.content;

                // Insérer chaque carte avant le premier élément de la liste
                container.insertBefore(cardContent, firstCard);
              });
        }
        // Effacer le champ de saisie
        input.value = "";
        }


    )
    .catch(function (error) {
        console.error('Il y a eu un problème avec l\'opération fetch : ' + error.message);
    });




}

function clickButtonAleatoire(event){
    // pour eviter que le formulaire se recharge:
    event.preventDefault()


    // requête AJAX
    const url = `${urlTest}`
    console.log(url)

    fetch(url)
    // Le paramètre response prend la valeur de l’objet renvoyé de fetch(url). Utilisez la méthode json() pour convertir response en données JSON.
    .then((response) => response.json())
    .then(
        function(data) {
        console.log(data);
        console.log(data.meals[0].strMeal,data.meals[0].strCategory)
        //    Cette ligne de code utilise la syntaxe de décomposition ("destructuring" en anglais) en JavaScript pour extraire des valeurs d'un objet et les affecter à des variables distinctes. Plus précisément, cette ligne extrait les propriétés main, name, sys et weather de l'objet data, et les affecte respectivement aux variables du même nom.
        const {meals, strMeal, strCategory, strArea, strMealThumb} = data
        console.log(meals, meals[0].strMeal, meals[0].strCategory, meals[0].strArea, meals[0].strMealThumb)
        const container = document.querySelector('.cities');
        const firstCard = container.firstChild; // récupère le premier élément de la liste

        // Créer une instance de RecetteDetails
        const recetteDetails = new RecetteDetails(meals);

        // Récupérer le contenu de la carte
        const cardContent = recetteDetails.content;

        // Insérer la nouvelle carte avant le premier élément de la liste, c'est mieux pour la version mobile
        container.insertBefore(cardContent, firstCard);
        }
    )
    .catch(function (error) {
        console.error('Il y a eu un problème avec l\'opération fetch : ' + error.message);
    });

}








