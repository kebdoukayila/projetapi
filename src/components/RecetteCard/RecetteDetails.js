import { RecetteDescription } from "./RecetteDescription.js";


export class RecetteDetails {
  _meals;

  constructor(meals) {
    this._meals = meals;
    console.log("Nouvelle Recette", this._meals);
  }

  // on crée un tableau pour les ingrédients et les mesures pour tous les récupérer
  getIngredientsAndMeasures() {
    const ingredients = [];
    const measures = [];

    // Parcourir les propriétés de l'objet de recette
    for (const key in this._meals[0]) {
      // Si la propriété commence par "strIngredient", ajouter sa valeur au tableau des ingrédients
      if (key.startsWith("strIngredient") && this._meals[0][key]) {
        ingredients.push(this._meals[0][key]);
      }
      // Si la propriété commence par "strMeasure", ajouter sa valeur au tableau des mesures
      if (key.startsWith("strMeasure") && this._meals[0][key]) {
        measures.push(this._meals[0][key]);
      }
    }

    // Concaténer les ingrédients et les mesures dans une chaîne
    let ingredientsAndMeasures = "";
    for (let i = 0; i < ingredients.length; i++) {
      ingredientsAndMeasures += `${measures[i]} ${ingredients[i]}<br>`;
    }
    return ingredientsAndMeasures;
  }


  showDetails(event) {
    if (event && event.target && event.target.classList.contains("boutonrecette")) {
      const detailsWindow = window.open(
        "recetteDescription.html",
        "Détails de la recette",
        "width=700,height=500,top=100,left=100,scrollbars=yes"
      );

      const details = `
        <img class="city-icon" src="${this._meals[0].strMealThumb}"></br>
        <h1 class="city-temp" style="text-align: center;">${this._meals[0].strMeal}</h1></br>
        <h2 style="margin: 0 auto; text-align: center;">${this._meals[0].strArea}</h2>
        <h2 style="margin: 0 auto; text-align: center;">${this._meals[0].strCategory}</h2></br>
        <p>${this.getIngredientsAndMeasures()}</p></br>
        <p style="text-align: justify;">${this._meals[0].strInstructions}</p></br>
        <button class="boutonrecette"><a href="${this._meals[0].strYoutube}" style="text-align: center;">Voir la recette sur youtube</a></button>
      `;

      detailsWindow.onload = () => {
        const detailsDiv = detailsWindow.document.getElementById("details");
        detailsDiv.innerHTML = details;
      };
      console.log("jusqu'ici tout vas bien");
    }
  }

  get content() {
    const li = document.createElement("li");
    li.classList.add("city");
    const contenuHtml = `
      <img class="city-icon" src="${this._meals[0].strMealThumb}">
      <div class="city-temp">${this._meals[0].strMeal}</div>
      <h2 style="margin: 0 auto; text-align: center;">${this._meals[0].strArea}</h2>
      <h2 style="margin: 0 auto; text-align: center;">${this._meals[0].strCategory}</h2>
      <button class="boutonrecette"><a id="${this._meals[0].idMeal}" class="boutonrecette" href="#">Voir</a></button
    `;
    li.innerHTML = contenuHtml;

    // fonction fléchée pour lier l'événement onclick à l'objet
    const voirButton = li.querySelector(".boutonrecette");
    voirButton.onclick = (event) => this.showDetails(event);

    return li;
  }
}



