/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/  "use strict";
/******/  var __webpack_modules__ = ({

/***/ "./src/components/RecetteCard/RecetteDescription.js":
/*!**********************************************************!*\
  !*** ./src/components/RecetteCard/RecetteDescription.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"RecetteDescription\": () => (/* binding */ RecetteDescription)\n/* harmony export */ });\n/* harmony import */ var _RecetteDetails_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RecetteDetails.js */ \"./src/components/RecetteCard/RecetteDetails.js\");\n\r\n\r\n\r\n\r\nclass RecetteDescription {\r\n    _meals;\r\n\r\n  \r\n    constructor(meals) {\r\n      this._meals = meals;\r\n\r\n  \r\n      console.log(\r\n        \"Nouvelle Recette\",\r\n        this._meals,\r\n\r\n      );\r\n  \r\n    }\r\n  \r\n    // get content() {\r\n    //     const li = document.createElement(\"li\")\r\n    //     li.classList.add(\"city\")\r\n    //     const contenuHtml =\r\n    //     `\r\n    //     <img class=\"city-icon\" src=\"${this._meals[0].strMealThumb}\">\r\n    //     <div class=\"city-temp\">${this._meals[0].strMeal}</div>\r\n    //     <h2 style=\"margin: 0 auto; text-align: center;\">${this._meals[0].strArea}</h2>\r\n    //     <h2 style=\"margin: 0 auto; text-align: center;\">${this._meals[0].strCategory}</h2> \r\n    //     <p>${this._meals[0].strInstructions}</p>  \r\n    //     <p>${this._meals[0].strIngredient}</p>\r\n    //     <p>${this._meals[0].strstrMeasure}</p>  \r\n    //     <p>${this._meals[0].strYoutube}</p>\r\n    //     `\r\n    //     li.innerHTML = contenuHtml\r\n    //     return li\r\n    // }\r\n  }\n\n//# sourceURL=webpack://empty-project/./src/components/RecetteCard/RecetteDescription.js?");

/***/ }),

/***/ "./src/components/RecetteCard/RecetteDetails.js":
/*!******************************************************!*\
  !*** ./src/components/RecetteCard/RecetteDetails.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"RecetteDetails\": () => (/* binding */ RecetteDetails)\n/* harmony export */ });\n/* harmony import */ var _RecetteDescription_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RecetteDescription.js */ \"./src/components/RecetteCard/RecetteDescription.js\");\n\r\n\r\n\r\nclass RecetteDetails {\r\n  _meals;\r\n\r\n  constructor(meals) {\r\n    this._meals = meals;\r\n    console.log(\"Nouvelle Recette\", this._meals);\r\n  }\r\n\r\n  // on crée un tableau pour les ingrédients et les mesures pour tous les récupérer\r\n  getIngredientsAndMeasures() {\r\n    const ingredients = [];\r\n    const measures = [];\r\n\r\n    // Parcourir les propriétés de l'objet de recette\r\n    for (const key in this._meals[0]) {\r\n      // Si la propriété commence par \"strIngredient\", ajouter sa valeur au tableau des ingrédients\r\n      if (key.startsWith(\"strIngredient\") && this._meals[0][key]) {\r\n        ingredients.push(this._meals[0][key]);\r\n      }\r\n      // Si la propriété commence par \"strMeasure\", ajouter sa valeur au tableau des mesures\r\n      if (key.startsWith(\"strMeasure\") && this._meals[0][key]) {\r\n        measures.push(this._meals[0][key]);\r\n      }\r\n    }\r\n\r\n    // Concaténer les ingrédients et les mesures dans une chaîne\r\n    let ingredientsAndMeasures = \"\";\r\n    for (let i = 0; i < ingredients.length; i++) {\r\n      ingredientsAndMeasures += `${measures[i]} ${ingredients[i]}<br>`;\r\n    }\r\n    return ingredientsAndMeasures;\r\n  }\r\n\r\n\r\n  showDetails(event) {\r\n    if (event && event.target && event.target.classList.contains(\"boutonrecette\")) {\r\n      const detailsWindow = window.open(\r\n        \"recetteDescription.html\",\r\n        \"Détails de la recette\",\r\n        \"width=700,height=500,top=100,left=100,scrollbars=yes\"\r\n      );\r\n\r\n      const details = `\r\n        <img class=\"city-icon\" src=\"${this._meals[0].strMealThumb}\"></br>\r\n        <h1 class=\"city-temp\" style=\"text-align: center;\">${this._meals[0].strMeal}</h1></br>\r\n        <h2 style=\"margin: 0 auto; text-align: center;\">${this._meals[0].strArea}</h2>\r\n        <h2 style=\"margin: 0 auto; text-align: center;\">${this._meals[0].strCategory}</h2></br>\r\n        <p>${this.getIngredientsAndMeasures()}</p></br>   \r\n        <p style=\"text-align: justify;\">${this._meals[0].strInstructions}</p></br>  \r\n        <button class=\"boutonrecette\"><a href=\"${this._meals[0].strYoutube}\" style=\"text-align: center;\">Voir la recette sur youtube</a></button>            \r\n      `;\r\n\r\n      detailsWindow.onload = () => {\r\n        const detailsDiv = detailsWindow.document.getElementById(\"details\");\r\n        detailsDiv.innerHTML = details;\r\n      };\r\n      console.log(\"jusqu'ici tout vas bien\");\r\n    }\r\n  }\r\n\r\n  get content() {\r\n    const li = document.createElement(\"li\");\r\n    li.classList.add(\"city\");\r\n    const contenuHtml = `\r\n      <img class=\"city-icon\" src=\"${this._meals[0].strMealThumb}\">\r\n      <div class=\"city-temp\">${this._meals[0].strMeal}</div>\r\n      <h2 style=\"margin: 0 auto; text-align: center;\">${this._meals[0].strArea}</h2>\r\n      <h2 style=\"margin: 0 auto; text-align: center;\">${this._meals[0].strCategory}</h2>  \r\n      <button class=\"boutonrecette\"><a id=\"${this._meals[0].idMeal}\" class=\"boutonrecette\" href=\"#\">Voir</a></button            \r\n    `;\r\n    li.innerHTML = contenuHtml;\r\n    \r\n    // fonction fléchée pour lier l'événement onclick à l'objet\r\n    const voirButton = li.querySelector(\".boutonrecette\");\r\n    voirButton.onclick = (event) => this.showDetails(event);\r\n    \r\n    return li;\r\n  }\r\n}\r\n\r\n\r\n\r\n \r\n\r\n\r\n\n\n//# sourceURL=webpack://empty-project/./src/components/RecetteCard/RecetteDetails.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _components_RecetteCard_RecetteDetails_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/RecetteCard/RecetteDetails.js */ \"./src/components/RecetteCard/RecetteDetails.js\");\n/* harmony import */ var _components_RecetteCard_RecetteDescription_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/RecetteCard/RecetteDescription.js */ \"./src/components/RecetteCard/RecetteDescription.js\");\n\r\n\r\n\r\n\r\nconst apiKey = \"1\";\r\n\r\n\r\n\r\nconst urlTest = \"https://www.themealdb.com/api/json/v1/1/random.php\"\r\n\r\n// const paramsUrl = \"&units=metric&lang=fr\"\r\n\r\n\r\nconst envoyer = document.querySelector(\".top-banner form\")\r\nenvoyer.addEventListener(\"submit\", rechercherRecette)\r\nconst input = document.querySelector(\".top-banner input\")\r\n\r\nconst rechercheAleatoire = document.querySelector(\".top-banner .buttonAleatoire\")\r\nrechercheAleatoire.addEventListener(\"click\", clickButtonAleatoire)\r\n\r\nconst urlSearchByName = \"https://www.themealdb.com/api/json/v1/1/search.php?s=\"\r\n\r\n\r\nfunction rechercherRecette(event){\r\n    event.preventDefault()\r\n\r\n    // recuperer la valeur du input\r\n    let inputValue = input.value\r\n    // requête AJAX\r\n    const url2 = `${urlSearchByName}${inputValue}`\r\n    console.log(url2)\r\n\r\n    fetch(url2)\r\n    // Le paramètre response prend la valeur de l’objet renvoyé de fetch(url). Utilisez la méthode json() pour convertir response en données JSON.\r\n    .then((response) => response.json())\r\n    .then(\r\n        function(data) {\r\n        console.log(data);\r\n        console.log(data.meals[0].strMeal,data.meals[0].strCategory)\r\n        //    Cette ligne de code utilise la syntaxe de décomposition (\"destructuring\" en anglais) en JavaScript pour extraire des valeurs d'un objet et les affecter à des variables distinctes. Plus précisément, cette ligne extrait les propriétés main, name, sys et weather de l'objet data, et les affecte respectivement aux variables du même nom.\r\n        const {meals, strMeal, strCategory, strArea, strMealThumb} = data\r\n        console.log(meals, meals[0].strMeal, meals[0].strCategory, meals[0].strArea, meals[0].strMealThumb, meals[0].idMeal)\r\n        const container = document.querySelector('.cities');\r\n        const firstCard = container.firstChild; // récupère le premier élément de la liste\r\n        \r\n        // on utilise forEach pour afficher les recettes qui ont le même nom par exemple si l'utilisateur écrit \"curry\" alors toutes les recettes avec le mot curry sortiront\r\n        if (!inputValue){\r\n            alert(\"Veuillez renseigner une recette\");\r\n        }else{\r\n            meals.forEach((meal) => {\r\n                // Créer une instance de RecetteDetails pour chaque recette\r\n                const recetteDetails = new _components_RecetteCard_RecetteDetails_js__WEBPACK_IMPORTED_MODULE_0__.RecetteDetails([meal]);\r\n                \r\n                // Récupérer le contenu de la carte pour chaque recette\r\n                const cardContent = recetteDetails.content;\r\n                \r\n                // Insérer chaque carte avant le premier élément de la liste\r\n                container.insertBefore(cardContent, firstCard);\r\n              });\r\n        }\r\n        // Effacer le champ de saisie\r\n        input.value = \"\";\r\n        }\r\n        \r\n        \r\n    )\r\n    .catch(function (error) {\r\n        console.error('Il y a eu un problème avec l\\'opération fetch : ' + error.message);\r\n    });\r\n\r\n\r\n    \r\n\r\n}\r\n\r\nfunction clickButtonAleatoire(event){\r\n    // pour eviter que le formulaire se recharge:\r\n    event.preventDefault()\r\n\r\n\r\n    // requête AJAX\r\n    const url = `${urlTest}`\r\n    console.log(url)\r\n\r\n    fetch(url)\r\n    // Le paramètre response prend la valeur de l’objet renvoyé de fetch(url). Utilisez la méthode json() pour convertir response en données JSON.\r\n    .then((response) => response.json())\r\n    .then(\r\n        function(data) {\r\n        console.log(data);\r\n        console.log(data.meals[0].strMeal,data.meals[0].strCategory)\r\n        //    Cette ligne de code utilise la syntaxe de décomposition (\"destructuring\" en anglais) en JavaScript pour extraire des valeurs d'un objet et les affecter à des variables distinctes. Plus précisément, cette ligne extrait les propriétés main, name, sys et weather de l'objet data, et les affecte respectivement aux variables du même nom.\r\n        const {meals, strMeal, strCategory, strArea, strMealThumb} = data\r\n        console.log(meals, meals[0].strMeal, meals[0].strCategory, meals[0].strArea, meals[0].strMealThumb)\r\n        const container = document.querySelector('.cities');\r\n        const firstCard = container.firstChild; // récupère le premier élément de la liste\r\n        \r\n        // Créer une instance de RecetteDetails\r\n        const recetteDetails = new _components_RecetteCard_RecetteDetails_js__WEBPACK_IMPORTED_MODULE_0__.RecetteDetails(meals);\r\n        \r\n        // Récupérer le contenu de la carte\r\n        const cardContent = recetteDetails.content;\r\n        \r\n        // Insérer la nouvelle carte avant le premier élément de la liste, c'est mieux pour la version mobile\r\n        container.insertBefore(cardContent, firstCard);\r\n        } \r\n    )\r\n    .catch(function (error) {\r\n        console.error('Il y a eu un problème avec l\\'opération fetch : ' + error.message);\r\n    });\r\n\r\n}\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\n\n//# sourceURL=webpack://empty-project/./src/index.js?");

/***/ })

/******/  });
/************************************************************************/
/******/  // The module cache
/******/  var __webpack_module_cache__ = {};
/******/
/******/  // The require function
/******/  function __webpack_require__(moduleId) {
/******/    // Check if module is in cache
/******/    var cachedModule = __webpack_module_cache__[moduleId];
/******/    if (cachedModule !== undefined) {
/******/      return cachedModule.exports;
/******/    }
/******/    // Create a new module (and put it into the cache)
/******/    var module = __webpack_module_cache__[moduleId] = {
/******/      // no module.id needed
/******/      // no module.loaded needed
/******/      exports: {}
/******/    };
/******/
/******/    // Execute the module function
/******/    __webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/
/******/    // Return the exports of the module
/******/    return module.exports;
/******/  }
/******/
/************************************************************************/
/******/  /* webpack/runtime/define property getters */
/******/  (() => {
/******/    // define getter functions for harmony exports
/******/    __webpack_require__.d = (exports, definition) => {
/******/      for(var key in definition) {
/******/        if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/          Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/        }
/******/      }
/******/    };
/******/  })();
/******/
/******/  /* webpack/runtime/hasOwnProperty shorthand */
/******/  (() => {
/******/    __webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/  })();
/******/
/******/  /* webpack/runtime/make namespace object */
/******/  (() => {
/******/    // define __esModule on exports
/******/    __webpack_require__.r = (exports) => {
/******/      if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/        Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/      }
/******/      Object.defineProperty(exports, '__esModule', { value: true });
/******/    };
/******/  })();
/******/
/************************************************************************/
/******/
/******/  // startup
/******/  // Load entry module and return exports
/******/  // This entry module can't be inlined because the eval devtool is used.
/******/  var __webpack_exports__ = __webpack_require__("./src/index.js");
/******/
/******/ })()
;
